# twitch-playlists

Get links for the Twitch VOD and streams.

## Installation

```bash
nimble install https://gitlab.com/artemklevtsov/nim-twitch-playlists
```

## Usage

```bash
export PATH=$PATH:$HOME/.nimble/bin/
twitch-playlists https://www.twitch.tv/vovapain
twitch-playlists https://www.twitch.tv/videos/1447563654
```
