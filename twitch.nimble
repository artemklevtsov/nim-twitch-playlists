# Package

version       = "1.0.0"
author        = "Artem Klevtsov"
description   = "Download Twtch.tv VODs"
license       = "MIT"
srcDir        = "src"
namedBin["playlists"] = "twitch-playlists"


# Dependencies

requires "nim >= 1.6.4"
