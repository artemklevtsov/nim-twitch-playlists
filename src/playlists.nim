import std/os
import std/strutils
import twitch/utils
import twitch/playlist
import twitch/stream
import twitch/vod

when isMainModule:
  if paramCount() != 1:
    let appName = extractFilename(getAppFilename())
    let msg = "Usage:\n" &
              "  " & appName & " <CHANNEL>\n" &
              "  " & appName & " <VODID>\n" &
              "  " & appName & " <URL>\n"
    quit(msg, QuitSuccess)
  let input = getEndpoint(paramStr(1))
  if allCharsInSet(input, Digits):
    echo $getVODPLaylist(input.parseInt())
  else:
    echo $getStreamPlaylist(input)
