import std/strutils
import std/json
import std/httpclient
import consts

type
  Token* = object
    token*: string
    sig*: string

proc getToken(id: string, isVOD: bool): JsonNode =
  const url = API_TOKEN_URL
  let body = %* {
    "operationName": "PlaybackAccessToken",
    "variables": {
      "isLive": not isVOD,
      "isVod": isVOD,
      "login": if not isVOD: id else: "",
      "vodID": if isVOD: id else: "",
      "playerType": "channel_home_live"
    },
    "extensions": {
      "persistedQuery": {
        "version": 1,
        "sha256Hash": API_TOKEN_HASH
      }
    }
  }
  let cl = newHttpClient()
  cl.headers = newHttpHeaders({
    "Content-Type": "text/plain;charset=UTF-8",
    "Client-ID": API_CLIENT_ID
  })
  let resp = cl.post(url, $body)
  parseJson(resp.body)["data"]

proc parseToken(data: JsonNode, key: string): Token =
  Token(
    token: data[key]["value"].getStr().replace("\\", ""),
    sig: data[key]["signature"].getStr()
  )

proc getStreamToken*(channelName: string = ""): Token =
  ## Get Access Token for the VOD or Stream.
  getToken(channelName, false).parseToken("streamPlaybackAccessToken")

proc getVODToken*(vodId: string = ""): Token =
  ## Get Access Token for the VOD or Stream.
  getToken($vodId, true).parseToken("videoPlaybackAccessToken")
