import std/uri
import std/httpclient
import consts
import token
import playlist

proc getStreamPlaylist*(channelName: string): PlayList =
  ## Get stream playlist URL.
  let token = getStreamToken(channelName)
  let query = {
    "allow_source": "true",
    "allow_audio_only": "true",
    "sig": token.sig,
    "token": token.token
  }
  let url = parseUri(PLAYLIST_URL) / "api/channel/hls" / (channelName & ".m3u8") ? query
  let cl = newHttpClient()
  let resp = cl.get($url)
  case resp.code:
    of Http200:
      parsePlaylist(resp.body)
    else:
      raise newException(IOError, "Can not find such stream.")
