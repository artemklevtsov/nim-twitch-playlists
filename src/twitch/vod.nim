import std/strutils
import std/sequtils
import std/json
import std/uri
import std/httpclient
import std/asyncdispatch
import consts
import token
import playlist

proc getDomains(): seq[Uri] =
  ## Get Twitch VOD domains.
  newHttpClient().getContent(DOMAINS_LIST_URL).splitLines().map(parseUri)

proc findDomain(path: string): Uri =
  ## Find correct domain for the path.
  let domains {.global.} = getDomains()
  var futures: seq[Future[AsyncResponse]]
  for i in 0 .. domains.len - 2:
    futures.add newAsyncHttpClient().get(domains[i] / path)
  discard waitFor all(futures)
  for i in 0 .. futures.len - 1:
    if futures[i].read.code == Http200:
      return domains[i]

proc getVODPath(vodId: int64): string =
  ## Get VOD URL path.
  let url = "https://api.twitch.tv/kraken/videos/" & $vodId
  let cl = newHttpClient()
  cl.headers = newHttpHeaders({
    "Content-Type": "text/plain; charset=UTF-8",
    "Accept": API_ACCEPT,
    "Client-ID": API_CLIENT_ID
  })
  let resp = cl.get(url)
  let respBody = parseJson(resp.body)
  respBody["seek_previews_url"].getStr().split('/')[3]

proc getVideoPLaylistSub(vodId: int64, token: Token): PlayList =
  ## Get VOD playlists URLs (for subscribers only).
  let path = getVODPath(vodId)
  let baseURL = findDomain(path & "/chunked/index-dvr.m3u8")
  if baseURL.hostname.len == 0:
    raise newException(IOError, "Can't find such video.")
  let qualities = parseJson(token.token)["chansub"]["restricted_bitrates"]
  for i in qualities:
    var entry: PlaylistEntry
    entry.quality = i.getStr()
    entry.url = $(baseURL / path / entry.quality / "index-dvr.m3u8")
    if entry.quality == "chunked":
      entry.quality = "source"
    result.add entry

proc getVODPLaylist*(vodId: int64): PlayList =
  ## Get VOD playlists URLs (for subscribers and nonsubscribers).
  let token = getVODToken($vodId)
  let query = {
    "allow_source": "true",
    "allow_audio_only": "true",
    "player": "twitchweb",
    "sig": token.sig,
    "token": token.token
  }
  let url = PLAYLIST_URL / "vod" / ($vodId & ".m3u8") ? query
  let cl = newHttpClient()
  let resp = cl.get($url)
  case resp.code:
    of Http200:
      parsePlaylist(resp.body)
    of Http403:
      getVideoPLaylistSub(vodId, token)
    else:
      raise newException(IOError, "Can not find such video.")
