import std/uri
import std/strutils

proc getEndpoint*(s: string): string =
  if '/' notin s:
    return s
  let u = parseUri(s)
  if u.hostname notin ["", "www.twitch.tv"]:
    raise newException(ValueError, "URL must contains www.twitch.tv.")
  let p = u.path.split('/')
  if "videos" in p: p[^1] else: p[1]
