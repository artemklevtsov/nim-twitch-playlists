import std/strutils

type
  PlaylistEntry* = object
    quality*: string
    url*: string
  PlayList* = seq[PlaylistEntry]

proc reset(e: var PlaylistEntry) =
  e.quality = ""
  e.url = ""

proc `$`*(p: PlayList): string =
  if p.len == 0:
    return
  for e in p:
    echo e.quality & ": " & e.url

proc extractQuality(s: string): string =
  for grp in s.split(','):
    let kv = grp.split('=', 1)
    if kv[0] == "VIDEO":
      let val = kv[1].strip(chars = {'"'})
      return if val == "chunked": "source" else: val

proc parsePlaylist*(s: string): PlayList =
  var entry: PlaylistEntry
  for line in s.splitLines():
    if line.startsWith("#EXT-X-STREAM-INF"):
      entry.quality =  extractQuality(line)
    if line.endsWith(".m3u8"):
      entry.url = line
    if entry.quality.len > 0 and entry.url.len > 0:
      result.add entry
      entry.reset
